#!/bin/bash
#==============================================================================#
# Este script permite a importação das mensagens de ROS que existam 
# num ambiente ROS
# 
# Depende da definição do ficheiro 'packages.list'
# Gera os jars para cada package na directoria messages-lib
#
#==============================================================================#




unset BUILD_DEPENDS

BASE_DIR=`pwd`
ROS_PKG_PATH='.'
MVN_REPO_DIR="./rosjava_mvn_repo"
ROS_MESSAGES_DIR="./rosjava_messages"
PKG_LIST_FILE="packages.list"
PKG_XML_FILE="package.xml"
JARS_LIBS_DIR="$BASE_DIR/message-libs"

source $PKG_LIST_FILE
# se não existe um repositorio cria a partir do git
if [ ! -d $MVN_REPO_DIR ]; then
    git clone https://github.com/rosjava/rosjava_mvn_repo.git
fi
# exporta variável de ambiente para gradle
export ROS_MAVEN_REPOSITORY=$(readlink -e $MVN_REPO_DIR)

# limpa a directoria de geracao das mensagens e cria a partir do git 
rm -rf $ROS_MESSAGES_DIR
git clone https://github.com/rosjava/rosjava_messages.git
cd $ROS_MESSAGES_DIR
# repositiona-se no branch adequado 'hydro'
git checkout -b hydro origin/hydro

# hack para evitar um erro de compilação existente no gradle
mkdir -p ./rosjava_test_msgs/build/classes/main


# carrega as definições para criacao de mensagens dos packages ros

for package in ${package_list[@]}; do
    if [ ! -z ${!package} ];then
	# adiciona a variàvel de ambiente 
        ROS_PKG_PATH=$ROS_PKG_PATH:${!package};
    fi
    # constroi instruções de dependencia para o 'package.xml'
    BUILD_DEPENDS="${BUILD_DEPENDS}<build_depend>${package}</build_depend>"
done
# cria um ficheiro package.xml customizado
cat  << _EOF_ > $PKG_XML_FILE
<?xml version="1.0"?>
<package>
  <name>rosjava_messages</name>
  <version>$version</version>
  <description>
	Seagull Message generation for rosjava.
  </description>
  <maintainer email="posilva@academiafa.edu.pt">Pedro Marques da Silva</maintainer>
  <license>Apache 2.0</license>
  <buildtool_depend>catkin</buildtool_depend>
  <build_depend>rosjava_build_tools</build_depend>
  <build_depend>rosjava_bootstrap</build_depend>
  <build_depend>map_store</build_depend> <!-- ach - mixes msgs and needed for android apps -->
  $BUILD_DEPENDS	
  <run_depend>rosjava_build_tools</run_depend>
  <run_depend>rosjava_bootstrap</run_depend>
</package>

_EOF_

# actualiza a variavel de ambniente com os caminhos dos packages
export ROS_PACKAGE_PATH=$ROS_PKG_PATH
# cria directoria para producao das bibliotecas 
mkdir -p $JARS_LIBS_DIR

# compila o rosjava_messages
./gradlew build

# copia as bibliotecas geradas para a directoria de output
for package in ${package_list[@]}; do
    cp ./${package}/build/libs/*.jar $JARS_LIBS_DIR
done

